#!/usr/bin/python3
from datetime import datetime
from datetime import timedelta
import fire
import praw

import secrets


DEFAULT_MAX_SUBMISSION_LIMIT = 500

reddit = praw.Reddit(client_id=secrets.credentials['client_id'],
                     client_secret=secrets.credentials['client_secret'],
                     user_agent=secrets.credentials['user_agent'],
                     username=secrets.credentials['username'],
                     password=secrets.credentials['password'])

def create_url(subreddit_name, submission_id):
    """Creates a url from the a subreddit and submission_id
    """
    return 'https://www.reddit.com/r/{}/comments/{}/'.format(subreddit_name, submission_id)


def get_todays_posts_from_subreddit(subreddit_name, filter_string='', submission_limit=DEFAULT_MAX_SUBMISSION_LIMIT):
    """Grabs the 500 newest posts and returns the ones from today

    Args:
        subreddit_name (str): the name of the subreddit to get posts from
        submission_name (int): the number of the submissions to attempt to query

    Returns:
        a list of useful submission_info tuples
            Format: (submission.title, submission.id, submission_created_utc, subreddit_name)
    """
    subreddit = reddit.subreddit(subreddit_name)
    submission_data = [(submission.title, submission.id, submission.created_utc, subreddit_name) for submission in subreddit.new(limit=submission_limit)]
    valid_submissions = []

    for submission in submission_data:
        if datetime.date(datetime.fromtimestamp(submission[2])) == datetime.date(datetime.utcnow()) or datetime.date(datetime.fromtimestamp(submission[2])) + timedelta(days=1) == datetime.date(datetime.utcnow()):
            valid_submissions.append(submission)

    return filter_by_title(valid_submissions, filter_string)


def get_todays_posts_from_subreddit_urls(subreddit_name, filter_string='', submission_limit=DEFAULT_MAX_SUBMISSION_LIMIT):
    """Prints out the urls from todays interesting posts

    Args:
        subreddit_name (str): the name of the subreddit to get posts from
        submission_name (int): the number of the submissions to attempt to query

    Returns:
        None
    """
    for post in get_todays_posts_from_subreddit(subreddit_name, filter_string, submission_limit):
        print(create_url(post[3], post[1]))


def filter_by_title(submissions, filter_string):
    """Creates a list of useful submission_info tuples that only include the provided filter string.

    Args:
        submissions (list): a list of submissions
            submission format: [submission.title, submission.id, submission.created_utc, subreddit_name/redditor_name]
        filter_string (str): the substring a submission title must contain

    Returns:
        A list of useful submission_info tuples that only include the provided filter string.
    """
    return [submission for submission in submissions if filter_string in submission[0]]


def save_interesting_posts(submission_ids):
    """Given a list of submission ids saves them to the user's account

    Args:
        submission_ids (list): a list of submission ids to save
        
    Returns:
        None
    """
    for submission_id in submission_ids:
        reddit.submission(id=submission_id).save()


def save_todays_interesting_posts_from_subreddit(subreddit_name, filter_string='', submission_limit=DEFAULT_MAX_SUBMISSION_LIMIT):
    """Saves the interesting posts from a subreddit

    Args:
        subreddit_name (str): the name of the subreddit to scrape from
        filter_string (str): a string to filter the titles on
        submission_limit (int): the max number of submissions to filter through

    Returns:
        None
    """
    todays_posts = get_todays_posts_from_subreddit(subreddit_name, filter_string, submission_limit)
    post_ids = [post[1] for post in todays_posts]
    save_interesting_posts(post_ids)


if __name__ == '__main__':
    fire.Fire({'save_today_from_subreddit' : save_todays_interesting_posts_from_subreddit,
               'print_todays_posts_from_subreddit': get_todays_posts_from_subreddit_urls})
