# RedditReporter

Searching on Reddit is pretty not fun if you only care about a small subset of topics within a subreddit.
 With RedditReporter you can filter through the junk and save the stuff that you actually care about or just print the links to view them right away.
 Provide the subreddit and the filter string and get the useful stuff.

RedditReporter requires a config file in the directory of the project.

### secrets.py
```python3
credentials = { 'client_id':'<CLIENT_ID>',
                'client_secret':'<CLIENT_SECRET>',
                'user_agent':'<AGENT>',
                'username':'<USERNAME>',
                'password':'<PASSWORD>'}
```

The username and password are the credentials for your user account and are needed in order to save posts to your account, otherwise they can only be printed.

The client_id and client_secret require that you register the script with reddit dev. The user agent doesnt really matter that much.


```
Usage:       reddit_reporter.py 
             reddit_reporter.py today-from-subreddit <SUBREDDIT_NAME> [<FILTER_STRING] [<SUBMISSION_LIMIT>]
			 reddit_reporter.py print-todays-posts-from-subreddit <SUBREDDIT_NAME> [<FILTER_STRING>]
```

tested on python 3.6.5
